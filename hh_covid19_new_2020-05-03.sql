# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: hh_covid19_new
# Generation Time: 2020-05-03 14:34:18 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table covid19_puskesmas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `covid19_puskesmas`;

CREATE TABLE `covid19_puskesmas` (
  `Kd_Puskesmas` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Puskesmas` varchar(50) NOT NULL,
  `Alamat` text,
  PRIMARY KEY (`Kd_Puskesmas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `covid19_puskesmas` WRITE;
/*!40000 ALTER TABLE `covid19_puskesmas` DISABLE KEYS */;

INSERT INTO `covid19_puskesmas` (`Kd_Puskesmas`, `Nm_Puskesmas`, `Alamat`)
VALUES
	(1,'Puskesmas Matiti','Ds. Matiti, Kec. Dolok Sanggul'),
	(2,'Puskesmas Paranginan','Komp. Lapangan Merdeka, Kec. Paranginan'),
	(3,'Puskesmas Hutapaung','Jl. Sidikalang Hutapaung, Kec. Pollung'),
	(4,'Puskesmas Saitnihuta','Ds. Saitnihuta, Kec. Dolok Sanggul'),
	(5,'Puskesmas Sigompul','Kec. Lintong Nihuta'),
	(6,'Puskesmas Baktiraja','Senambela, Kec. Baktiraja'),
	(7,'Puskesmas Parlilitan','Jl. Pendidikan No. 34 Sihastonga, Kec. Parlilitan'),
	(8,'Puskesmas Hutagalung','Jl. Sionom Hudon, Desa Sion Tonga, Kec. Parlilitan'),
	(9,'Puskesmas Pakkat','Jl. Sehat, Kec. Pakkat'),
	(10,'Puskesmas Onan Ganjang','Kec. Onan Ganjang'),
	(11,'Puskesmas Bonan Dolok','Bonandolok I, Kec. Sijama Polang'),
	(12,'Puskesmas Tarabintang','Kec. Tarabintang');

/*!40000 ALTER TABLE `covid19_puskesmas` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
