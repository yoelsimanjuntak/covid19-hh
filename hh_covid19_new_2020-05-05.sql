# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: hh_covid19_new
# Generation Time: 2020-05-04 18:21:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table covid19_data_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `covid19_data_detail`;

CREATE TABLE `covid19_data_detail` (
  `id` varchar(222) NOT NULL,
  `nik` varchar(222) DEFAULT NULL,
  `nama` varchar(222) NOT NULL,
  `tempat_lahir` varchar(222) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `luar_hh` text NOT NULL,
  `id_kec` char(55) NOT NULL,
  `id_desa` char(55) NOT NULL,
  `id_puskesmas` char(55) NOT NULL DEFAULT '',
  `dusun` varchar(222) NOT NULL,
  `no_hp` char(55) NOT NULL,
  `suhu_badan` char(55) NOT NULL,
  `keterangan` text NOT NULL,
  `foto_ktp` text NOT NULL,
  `foto_orang` text NOT NULL,
  `jenis` enum('MASUK','KELUAR','LAPOR') NOT NULL,
  `kesehatan` varchar(222) NOT NULL COMMENT 'batas koma untuk Demam • Pilek • Batuk • Sakit Tenggorokan • Sesak Nafas',
  `catatan_medis` text NOT NULL,
  `catatan_akhir` text,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `v_kecamatan` varchar(222) NOT NULL,
  `v_desa` varchar(222) NOT NULL,
  `v_puskesmas` varchar(222) DEFAULT NULL,
  `status` enum('VERIFIKASI','PEMANTAUAN','PEMANTAUAN_SELESAI') NOT NULL,
  `status_akhir` enum('OTG','ODP','PDP','POSITIF') NOT NULL,
  `tgl_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tgl_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `covid19_data_detail` WRITE;
/*!40000 ALTER TABLE `covid19_data_detail` DISABLE KEYS */;

INSERT INTO `covid19_data_detail` (`id`, `nik`, `nama`, `tempat_lahir`, `tgl_lahir`, `alamat`, `luar_hh`, `id_kec`, `id_desa`, `id_puskesmas`, `dusun`, `no_hp`, `suhu_badan`, `keterangan`, `foto_ktp`, `foto_orang`, `jenis`, `kesehatan`, `catatan_medis`, `catatan_akhir`, `lat`, `lng`, `v_kecamatan`, `v_desa`, `v_puskesmas`, `status`, `status_akhir`, `tgl_insert`, `tgl_update`)
VALUES
	('1','1271031208950002','Yoel R Simanjuntak','Medan','1995-08-12','Doloksanggul','','','','','','085359867032','36','DEV','','','MASUK','DEV','DEV','DEV',0,0,'','',NULL,'VERIFIKASI','','2020-05-05 01:19:18',NULL);

/*!40000 ALTER TABLE `covid19_data_detail` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table covid19_puskesmas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `covid19_puskesmas`;

CREATE TABLE `covid19_puskesmas` (
  `Kd_Puskesmas` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Puskesmas` varchar(50) NOT NULL,
  `Nm_Alamat` text,
  PRIMARY KEY (`Kd_Puskesmas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `covid19_puskesmas` WRITE;
/*!40000 ALTER TABLE `covid19_puskesmas` DISABLE KEYS */;

INSERT INTO `covid19_puskesmas` (`Kd_Puskesmas`, `Nm_Puskesmas`, `Nm_Alamat`)
VALUES
	(1,'Puskesmas Matiti','Ds. Matiti, Kec. Dolok Sanggul'),
	(2,'Puskesmas Paranginan','Komp. Lapangan Merdeka, Kec. Paranginan'),
	(3,'Puskesmas Hutapaung','Jl. Sidikalang Hutapaung, Kec. Pollung'),
	(4,'Puskesmas Saitnihuta','Ds. Saitnihuta, Kec. Dolok Sanggul'),
	(5,'Puskesmas Sigompul','Kec. Lintong Nihuta'),
	(6,'Puskesmas Baktiraja','Senambela, Kec. Baktiraja'),
	(7,'Puskesmas Parlilitan','Jl. Pendidikan No. 34 Sihastonga, Kec. Parlilitan'),
	(8,'Puskesmas Hutagalung','Jl. Sionom Hudon, Desa Sion Tonga, Kec. Parlilitan'),
	(9,'Puskesmas Pakkat','Jl. Sehat, Kec. Pakkat'),
	(10,'Puskesmas Onan Ganjang','Kec. Onan Ganjang'),
	(11,'Puskesmas Bonan Dolok','Bonandolok I, Kec. Sijama Polang'),
	(12,'Puskesmas Tarabintang','Kec. Tarabintang');

/*!40000 ALTER TABLE `covid19_puskesmas` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
