# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: hh_covid19
# Generation Time: 2020-04-07 15:15:56 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table covid19_kecamatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `covid19_kecamatan`;

CREATE TABLE `covid19_kecamatan` (
  `Kd_Kecamatan` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nm_Kecamatan` varchar(50) DEFAULT NULL,
  `Nm_Color` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Kd_Kecamatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `covid19_kecamatan` WRITE;
/*!40000 ALTER TABLE `covid19_kecamatan` DISABLE KEYS */;

INSERT INTO `covid19_kecamatan` (`Kd_Kecamatan`, `Nm_Kecamatan`, `Nm_Color`)
VALUES
	(1,'DOLOKSANGGUL','#021867'),
	(2,'LINTONG NI HUTA','#6e37b7'),
	(3,'BAKTIRAJA','#c3e7f8'),
	(4,'POLLUNG','#ef6b37'),
	(5,'PARLILITAN','#b22823'),
	(6,'PARANGINAN','#44a19e'),
	(7,'PAKKAT','#f9c647'),
	(8,'ONAN GANJANG','#339bf7'),
	(9,'TARABINTANG','#377d1c'),
	(10,'SIJAMAPOLANG','#45625e');

/*!40000 ALTER TABLE `covid19_kecamatan` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
