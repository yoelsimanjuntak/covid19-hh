<?php
class Home extends MY_Controller {

  public function index() {
    $data['title'] = 'Beranda';

    $this->load->model('mpost');

    $data['berita'] = $this->mpost->search(3,"",1);
    $data['notif'] = $this->mpost->search(5,"",2);
    $data['grafis'] = $this->mpost->search(5,"",3);

    $data['count_berita'] = $this->db
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, 1)
    ->where(TBL__POSTS.".".COL_ISSUSPEND, false)
    ->where(TBL__POSTS.".".COL_POSTEXPIREDDATE." >= ", date("Y-m-d"))
    ->count_all_results(TBL__POSTS);

    $data['count_notif'] = $this->db
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, 2)
    ->where(TBL__POSTS.".".COL_ISSUSPEND, false)
    ->where(TBL__POSTS.".".COL_POSTEXPIREDDATE." >= ", date("Y-m-d"))
    ->count_all_results(TBL__POSTS);

    $data['count_grafis'] = $this->db
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, 3)
    ->where(TBL__POSTS.".".COL_ISSUSPEND, false)
    ->where(TBL__POSTS.".".COL_POSTEXPIREDDATE." >= ", date("Y-m-d"))
    ->count_all_results(TBL__POSTS);
		$this->template->set('title', 'Home');
		$this->template->load('frontend' , 'home/index', $data);
    //$this->load->view('home/index');
  }

  public function page($slug) {
    $data['title'] = 'Page';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
        show_404();
        return false;
    }

    $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
    $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
    $this->db->update(TBL__POSTS);

    $data['title'] = $rpost[COL_POSTCATEGORYNAME];//strlen($rpost[COL_POSTTITLE]) > 50 ? substr($rpost[COL_POSTTITLE], 0, 50) . "..." : $rpost[COL_POSTTITLE];
    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/page', $data);
  }

  public function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('backend' , 'home/_error', $data);
    } else {
      $this->template->load('frontend' , 'home/_error', $data);
    }
  }

  public function post($cat) {
    $data['title'] = 'Tautan';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where(TBL__POSTS.".".COL_POSTCATEGORYID, $cat);
    $this->db->order_by(TBL__POSTS.".".COL_CREATEDON, 'desc');
    $data['res'] = $rpost = $this->db->get(TBL__POSTS)->result_array();

    if(!empty($rpost)) {
      $data['title'] = $rpost[0][COL_POSTCATEGORYNAME];
    }

    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/post', $data);
  }
}
 ?>
