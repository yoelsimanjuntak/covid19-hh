<?php
class Ajax extends MY_Controller {

  public function __construct()
  {
      parent::__construct();
      setlocale (LC_TIME, 'id_ID');
  }

  function now() {
    //echo date('D, d M Y H:i:s');

    $dt = json_encode(array(
      'Day'=>date('D'),
      'Date'=>date('d'),
      'Month'=>date('M'),
      'Year'=>date('Y'),
      'Hour'=>date('H'),
      'Minute'=>date('i'),
      'Second'=>date('s')
    ));
    echo $dt;
  }

  public function getdata() {
    $res = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_COVID19_DATA.".".COL_CREATE_BY,"left")
    ->order_by(COL_CREATE_DATE, 'desc')
    ->limit(1)
    ->get(TBL_COVID19_DATA)
    ->row_array();
    echo json_encode($res);
  }

  public function getdata_kec() {
    $q = @"
    select
    sum(tbl.Jlh_OTGP) as Jlh_OTGP,
    sum(tbl.Jlh_OTGS) as Jlh_OTGS,
    sum(tbl.Jlh_ODP) as Jlh_ODP,
    sum(tbl.Jlh_ODPS) as Jlh_ODPS,
    sum(tbl.Jlh_PDP) as Jlh_PDP,
    sum(tbl.Jlh_Positif) as Jlh_Positif,

    sum(tbl.Jlh_Suspect) as Jlh_Suspect,
    sum(tbl.Jlh_Probable) as Jlh_Probable,
    sum(tbl.Jlh_KontakErat) as Jlh_KontakErat,
    sum(tbl.Jlh_PelakuPerjalanan) as Jlh_PelakuPerjalanan,
    sum(tbl.Jlh_Discarded) as Jlh_Discarded,
    sum(tbl.Jlh_SelesaiIsolasi) as Jlh_SelesaiIsolasi,
    sum(tbl.Jlh_Kematian) as Jlh_Kematian,
    tbl.Create_Date,
    ifnull(tbl.Name, '--') as Name
    from (
          select
          kec.Kd_Kecamatan,
          kec.Nm_Kecamatan,
          ifnull((select dat.Jlh_OTGP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_OTGP,
          ifnull((select dat.Jlh_OTGS from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_OTGS,
          ifnull((select dat.Jlh_ODP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_ODP,
          ifnull((select dat.Jlh_ODPS from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_ODPS,
          ifnull((select dat.Jlh_PDP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_PDP,
          ifnull((select dat.Jlh_Positif from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Positif,

          ifnull((select dat.Jlh_Suspect from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Suspect,
          ifnull((select dat.Jlh_Probable from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Probable,
          ifnull((select dat.Jlh_KontakErat from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_KontakErat,
          ifnull((select dat.Jlh_PelakuPerjalanan from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_PelakuPerjalanan,
          ifnull((select dat.Jlh_Discarded from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Discarded,
          ifnull((select dat.Jlh_SelesaiIsolasi from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_SelesaiIsolasi,
          ifnull((select dat.Jlh_Kematian from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Kematian,
          (select dat.Create_Date from covid19_kecamatan_data dat order by dat.Create_Date desc limit 1) as Create_Date,
          (select u.Name from covid19_kecamatan_data dat left join _userinformation u on. u.UserName = dat.Create_By order by dat.Create_Date desc limit 1) as Name
          from covid19_kecamatan kec
    ) tbl
    ";
    $res = $this->db->query($q)->row_array();
    echo json_encode($res);
  }

  public function getpersebaran_kec() {
    $q = @"
    select * from (
      select
      kec.Kd_Kecamatan,
      kec.Nm_Kecamatan,
      kec.Nm_Color,
      ifnull((select dat.Jlh_OTGP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_OTGP,
      ifnull((select dat.Jlh_OTGS from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_OTGS,
      ifnull((select dat.Jlh_ODP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_ODP,
      ifnull((select dat.Jlh_ODPS from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_ODPS,
      ifnull((select dat.Jlh_PDP from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_PDP,
      ifnull((select dat.Jlh_Positif from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1), 0) as Jlh_Positif,
      (select dat.Create_Date from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1) as Create_Date,
      (select dat.Create_By from covid19_kecamatan_data dat where dat.Kd_Kecamatan = kec.Kd_Kecamatan order by dat.Create_Date desc limit 1) as Create_By
      from covid19_kecamatan kec
    ) tbl order by tbl.Nm_Kecamatan asc
    ";
    $res = $this->db->query($q)->result_array();
    $html = '';
    foreach($res as $r) {
      $html .= '<tr style="background-color:'.$r[COL_NM_COLOR].'; color: #fff"><td>'.$r[COL_NM_KECAMATAN].'</td><td class="text-right">'.number_format($r[COL_JLH_OTGP]+$r[COL_JLH_OTGS]).'</td><td class="text-right">'.number_format($r[COL_JLH_ODP]+$r[COL_JLH_ODPS]).'</td><td class="text-right">'.number_format($r[COL_JLH_PDP]).'</td><td class="text-right">'.number_format($r[COL_JLH_POSITIF]).'</td></tr>';
    }
    echo $html;
  }
}
