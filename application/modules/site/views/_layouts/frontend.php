
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">-->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>" />

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    <!-- datatable reorder _ buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- daterange picker -->
    <script src="<?=base_url()?>assets/js/moment.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.css">

    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>

    <!-- my css -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/css/styles.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/my.css">-->

    <script>
    function startTime() {
      /*$.get('<?=site_url('site/ajax/now')?>', function(data) {
        var res = JSON.parse(data);
        $('#datetime').html(res.Day.toUpperCase()+', '+res.Date+' '+res.Month.toUpperCase()+' '+res.Year+' <span class="text-danger">'+res.Hour+':'+res.Minute+':'+res.Second+'</span>');
      });*/
      var today = new Date();
      $('#datetime').html(moment(today).format('DD')+' '+moment(today).format('MMMM').toUpperCase()+' '+moment(today).format('Y')+' <span class="text-lime">'+moment(today).format('hh')+':'+moment(today).format('mm')+':'+moment(today).format('ss')+'</span>');
      var t = setTimeout(startTime, 1000);
    }
    $(document).ready(function() {
      startTime();
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      };
    });
    </script>
    <style>
    #footer-section::after {
      background: rgba(0, 0, 0, 0) url(<?=MY_IMAGEURL?>footer-map-bg.png) no-repeat scroll center center / 75% auto;
      content: "";
      height: 100%;
      left: 0;
      opacity: 0.1;
      position: absolute;
      top: 0;
      width: 100%;
      z-index: -1;
    }
    #footer-section table td {
      border-top: none !important;
    }
    </style>
</head>
<body class="hold-transition layout-top-nav layout-navbar-fixed">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-light navbar-white p-0 pb-2" style="display: block">
      <div class="row bg-red mb-2">
        <div class="container-fluid">
          <div class="row">
            <div class="mr-auto pl-3 d-none d-sm-inline-block">
              <i class="far fa-phone-square-alt"></i>&nbsp;Call Center : Dinas Kesehatan - 0822 7669 3066 | RSUD Doloksanggul - 0812 6370 0035
            </div>
            <div class="ml-auto pr-3">
              <span id="datetime"></span>
            </div>
          </div>

        </div>
      </div>
      <div class="container-fluid">
          <a href="<?=site_url()?>" class="navbar-brand">
              <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image elevation" style="opacity: .8">
              <span class="brand-text font-weight-light"><?=$this->setting_web_name?> <small class="d-none d-sm-inline-block"><?=$this->setting_web_desc?> <sup class="text-danger text-bold">ver <?=$this->setting_web_version?></sup></small></span>
          </a>

          <ul class="navbar-nav ml-auto mr-2">
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=site_url('site/home/post/1')?>" class="nav-link">Berita</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=site_url('site/home/post/2')?>" class="nav-link">Pemberitahuan</a>
            </li>
            <li class="nav-item d-sm-none">
                <a class="nav-link" href="#"><i class="fad fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=site_url('site/home/page/data-covid-19-indonesia-oleh-bnpb')?>" class="nav-link">Data &nbsp;<sup><span class="right badge badge-danger">BNPB</span></sup></a>
            </li>
          </ul>
      </div>
    </nav>
    <?=$content?>
    <div id="footer-section" style="position: relative; z-index:9; border-top: 1px solid #dee2e6;">
      <div class="content" style="padding: 0 .5rem">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-4 p-2">
              <div class="row">
                <div class="col-lg-4">
                  <p class="text-center">
                    <img class="mb-3" src="<?=MY_IMAGEURL.$this->setting_web_logo?>" height="80px" />
                  </p>
                </div>
                <div class="col-lg-8">
                  <p>
                    <b><?=$this->setting_org_name?></b><br  />
                    <?=$this->setting_org_address?>
                  </p>
                  <p>
                    Telp: <?=$this->setting_org_phone?>, Fax: <?=$this->setting_org_fax?><br  />
                    Email: <?=$this->setting_org_mail?>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 p-2">
              <form action="#">
                <h6>HUBUNGI KAMI</h6>
                <div class="col-lg-12">
                  <div class="form-group row">
                    <div class="col-lg-12">
                      <input type="text" class="form-control" placeholder="Nama Lengkap" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-6">
                      <input type="text" class="form-control" placeholder="No. Telp" />
                    </div>
                    <div class="col-lg-6">
                      <input type="email" class="form-control" placeholder="Email" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-12">
                      <textarea class="form-control" rows="3" placeholder="Pesan"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-12 text-right">
                      <button type="submit" class="btn btn-danger btn-sm"><i class="fad fa-pencil"></i> SUBMIT</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-lg-4 p-2">
              <h6>STATISTIK</h6>
              <div id="limb_counter">Loading Counter...</div>
              <meta content="text/html;charset=utf-8" http-equiv="Content-Type"><meta content="utf-8" http-equiv="encoding">
              <script>
                  var limb_width_counter ='100%';
                  var limb_height_counter ='130px';
                  var limb_id_counter = 'limb_counter';
              </script>
              </script>
              <script src="//codec.humbanghasundutankab.go.id/counter/counter.js"></script>
              <!--<div class="col-lg-12">
                <ul class="todo-list ui-sortable">
                  <li class="active" style="border-left: 2px solid #17a2b8">
                    <span class="text"><a class="text-info" href="https://tebingtinggikota.go.id/" target="_blank">Website Pemerintah Kota T. Tinggi</a></span>
                  </li>
                  <li class="active" style="border-left: 2px solid #17a2b8">
                    <span class="text"><a class="text-info" href="http://setda.tebingtinggikota.go.id/" target="_blank">Website Sekretariat Kota T. Tinggi</a></span>
                  </li>
                  <li class="active" style="border-left: 2px solid #17a2b8">
                    <span class="text"><a class="text-info" href="http://organisasi.tebingtinggikota.go.id/" target="_blank">Website Biro Organisasi Kota T. Tinggi</a></span>
                  </li>
                </ul>
              </div>-->
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <footer class="main-footer">
        <div class="float-right d-none d-sm-inline">
            <?php
            if(!IsLogin()) {
              ?>
              <a href="<?=site_url('site/user/login')?>" class="btn btn-outline-danger btn-sm"><i class="fad fa-sign-in"></i>&nbsp;&nbsp;Login</a>
              <?php
            } else {
              ?>
              <a href="<?=site_url('site/user/dashboard')?>" class="btn btn-outline-danger btn-sm"><i class="fad fa-tachometer-alt"></i>&nbsp;&nbsp;Dashboard</a>
              <?php
            }
            ?>
        </div>
        <strong>Copyright &copy; <?=date("Y")?> <?=$this->setting_web_name?></strong>. Strongly developed by <b>Partopi Tao</b>.
    </footer>
  </div>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.motio.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
      /*var element = document.querySelector('.content-wrapper');
      var panning = new Motio(element, {
        fps: 30,
        speedX: 30
      });
      panning.play();*/
  });
  </script>
</body>
</html>
