<style>
.todo-list>li:hover {
    background-color: #ccc;
}
.carousel-container {
  min-height: 300px;
}

#section-data .card-inner {
  transition: transform .2s;
}
#section-data .card-inner:hover {
  transform: scale(1.1);
  z-index: 1;
}
.timeline::before {
  display: none;
}
#section-data .content-header-bg::after {
  background: url(<?=MY_IMAGEURL.'footer-map-bg.png'?>) no-repeat scroll left top / 100% auto;
  content: "";
  height: 100%;
  left: 0;
  opacity: 0.1;
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 1;
}
</style>
<?php
$carouselDir =  scandir(MY_IMAGEPATH.'slide/');
$carouselArr = array();
foreach (scandir(MY_IMAGEPATH.'slide/') as $file) {
  if(strpos(mime_content_type(MY_IMAGEPATH.'slide/'.$file), 'image') !== false) {;
    $carouselArr[MY_IMAGEPATH.'slide/'.$file] = filemtime(MY_IMAGEPATH.'slide/'. $file); //$carouselArr[] = MY_IMAGEURL.'slide/'.$file;
  }
}
arsort($carouselArr);
$carouselArr = array_keys($carouselArr);

/*foreach($carouselDir as $c) {
  if(strpos(mime_content_type(MY_IMAGEPATH.'slide/'.$c), 'image') !== false) {
    $carouselArr[] = MY_IMAGEURL.'slide/'.$c;
  }
}*/
?>
<?php
if(!empty($carouselArr)) {
  ?>
  <div id="carouselMain" class="carousel slide carousel-fade d-none d-sm-block" style="margin-top: 5rem" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php
      for($i=0; $i<count($carouselArr); $i++) {
        ?>
        <li data-target="#carouselMain" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
        <?php
      }
      ?>
    </ol>
    <div class="carousel-inner">
      <?php
      for($i=0; $i<count($carouselArr); $i++) {
        ?>
        <div class="carousel-item <?=$i==0?'active':''?>">
          <img class="d-block w-100" src="<?=$carouselArr[$i]?>">
        </div>
        <?php
      }
      ?>
    </div>
    <a class="carousel-control-prev" href="#carouselMain" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselMain" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <?php
}
?>
<section id="section-data" class="content-wrapper mt-0" style="border-top: 2px solid #fff;">
  <section class="content-header bg-danger" style="position: relative">
    <div class="content-header-bg"></div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h3 class="font-weight-light">DATA PANTAUAN PENANGANAN COVID-19</h3>
          <h4>Kabupaten Humbang Hasundutan</h4>
        </div>
      </div>
    </div>
  </section>
  <section class="content mt-3">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <p class="text-center mb-0 font-italic">
            Data terakhir kali diupdate pada <span class="text-data text-data-last-updatedate font-weight-bold"></span> pukul <span class="text-data text-data-last-updatetime font-weight-bold"></span> <!--oleh <span class="text-data text-data-last-updateby font-weight-bold"></span>-->
          </p>
        </div>
      </div>
      <div class="row d-flex align-items-stretch p-3">
        <div class="col-12 col-sm-12 col-md-2 d-flex align-items-stretch">
          <div class="card card-outline card-info card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">Suspect</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h3 class="text-warning font-weight-bold text-data text-data-suspect">--</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-2 d-flex align-items-stretch">
          <div class="card card-outline card-info card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">Probable</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h3 class="text-warning font-weight-bold text-data text-data-probable">--</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-2 d-flex align-items-stretch">
          <div class="card card-outline card-success card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">Kontak Erat</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h3 class="text-warning font-weight-bold text-data text-data-kontakerat">--</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-2 d-flex align-items-stretch">
          <div class="card card-outline card-success card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">Pelaku Perjalanan</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h3 class="text-warning font-weight-bold text-data text-data-pelakuperjalanan">--</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-2 d-flex align-items-stretch">
          <div class="card card-outline card-warning card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">Discarded</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h3 class="text-warning font-weight-bold text-data text-data-discarded">--</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-2 d-flex align-items-stretch">
          <div class="card card-outline card-warning card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">Selesai Isolasi</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h3 class="text-warning font-weight-bold text-data text-data-selesaiisolasi">--</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4 offset-md-4 d-flex align-items-stretch">
          <div class="card card-outline card-danger card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">Kematian</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h3 class="text-warning font-weight-bold text-data text-data-kematian">--</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!--<div class="col-12 col-sm-12 col-md-3 d-flex align-items-stretch">
          <div class="card card-outline card-info card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">OTG<br /><small>(Orang Tanpa Gejala)</small></h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6 text-center">
                  <h3 class="text-warning font-weight-bold text-data text-data-otg-p">--</h3>
                  <span class="description-text">DALAM PEMANTAUAN</span>
                </div>
                <div class="col-sm-6 text-center">
                  <h3 class="text-success font-weight-bold text-data text-data-otg-s">--</h3>
                  <span class="description-text">SELESAI PEMANTAUAN</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-12 col-sm-12 col-md-3 d-flex align-items-stretch">
          <div class="card card-outline card-secondary card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">ODP<br /><small>(Orang Dalam Pemantauan)</small></h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6 text-center">
                  <h3 class="text-warning font-weight-bold text-data text-data-odp-p">--</h3>
                  <span class="description-text">DALAM PEMANTAUAN</span>
                </div>
                <div class="col-sm-6 text-center">
                  <h3 class="text-success font-weight-bold text-data text-data-odp-s">--</h3>
                  <span class="description-text">ODP SELESAI PEMANTAUAN</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-12 col-sm-12 col-md-3 d-flex align-items-stretch">
          <div class="card card-outline card-warning card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">PDP<br /><small>(Pasien Dalam Pengawasan)</small></h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h1 class="text-warning font-weight-bold text-data text-data-pdp">--</h1>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-12 col-sm-12 col-md-3 d-flex align-items-stretch">
          <div class="card card-outline card-danger card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">Positif<br />COVID-19</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h1 class="text-danger font-weight-bold text-data text-data-positif">--</h1>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row d-flex align-items-stretch p-3" style="display: none !important">
        <div class="col-12 col-sm-12 col-md-3 d-flex align-items-stretch">
          <div class="card card-outline card-secondary card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">OTG<br /><small>(Orang Tanpa Gejala)</small></h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h1 class="text-gray font-weight-bold text-data text-data-otg">--</h1>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-6">
                  <div class="description-block border-right">
                    <h5 class="description-header text-warning font-weight-bold text-data text-data-otg-p">--</h5>
                    <span class="description-text">DALAM PEMANTAUAN</span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="description-block">
                    <h5 class="description-header text-success font-weight-bold text-data text-data-otg-s">--</h5>
                    <span class="description-text">SELESAI PEMANTAUAN</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-3 d-flex align-items-stretch">
          <div class="card card-outline card-secondary card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">ODP<br /><small>(Orang Dalam Pemantauan)</small></h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h1 class="text-gray font-weight-bold text-data text-data-odp">--</strong></h1>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-6">
                  <div class="description-block border-right">
                    <h5 class="description-header text-warning font-weight-bold text-data text-data-odp-p">--</h5>
                    <span class="description-text">DALAM PEMANTAUAN</span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="description-block">
                    <h5 class="description-header text-success font-weight-bold text-data text-data-odp-s">--</h5>
                    <span class="description-text">SELESAI PEMANTAUAN</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-3 d-flex align-items-stretch">
          <div class="card card-outline card-warning card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">PDP<br /><small>(Pasien Dalam Pengawasan)</small></h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h1 class="text-warning font-weight-bold text-data text-data-pdp">--</h1>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="row d-flex align-items-stretch">
                <div class="col-sm-6">
                  <div class="description-block border-right">
                    <h5 class="description-header text-warning font-weight-bold text-data text-data-pdp-p">--</h5>
                    <span class="description-text">DALAM PENGAWASAN</span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="description-block">
                    <h5 class="description-header text-success font-weight-bold text-data text-data-pdp-s">--</h5>
                    <span class="description-text">SEMBUH</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-sm-12 col-md-3 d-flex align-items-stretch">
          <div class="card card-outline card-danger card-inner w-100 elevation-2">
            <div class="card-header text-center">
              <h5 class="card-title text-lg float-none">SUSPECT<br /><small>(Pasien Positif)</small></h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12 text-center">
                  <h1 class="text-danger font-weight-bold text-data text-data-suspect">--</h1>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-6">
                  <div class="description-block border-right">
                    <h5 class="description-header text-danger font-weight-bold text-data text-data-suspect-d">--</h5>
                    <span class="description-text">MENINGGAL DUNIA</span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="description-block">
                    <h5 class="description-header text-success font-weight-bold text-data text-data-suspect-r">--</h5>
                    <span class="description-text">SEMBUH</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>-->
      <!--<div class="row">
        <div class="col-sm-12">
          <div class="card card-outline">
            <div class="card-header">
              <h5 class="card-title">
                <small class="font-italic">Diupdate terakhir kali pada <strong><?=date('d-m-Y H:i:s')?></strong> oleh <strong>Admiral</strong></small>
              </h5>
              <div class="card-tools">
                <div class="card-tools">
                  <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i></button>
                </div>
              </div>
            </div>
            <div class="card-body">

            </div>
          </div>
        </div>
      </div>-->
      <div class="row">
        <div class="col-sm-6">
          <div class="card card-outline card-default">
            <div class="card-header">
              <h5 class="card-title">Berita</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <?php
                if(!empty($berita)) {
                  ?>
                  <div class="timeline timeline-inverse mb-0 w-100">
                    <?php
                    foreach($berita as $n) {
                      ?>
                      <div class="row m-0 mb-2">
                        <div class="timeline-item m-0 w-100">
                          <span class="time"><i class="fad fa-calendar"></i> <?=date('d-m-Y', strtotime($n[COL_POSTDATE]))?></span>
                          <h3 class="timeline-header font-weight-bold"><a href="<?=site_url('site/home/page/'.$n[COL_POSTSLUG])?>"><?=$n[COL_POSTTITLE]?></a></h3>
                          <div class="timeline-body">
                            <?php
                            $rimages = $this->db->where(COL_POSTID, $n[COL_POSTID])->limit(4)->get(TBL__POSTIMAGES)->result_array();
                            if(!empty($rimages)) {
                              ?>
                              <div class="row">
                                <div class="col-sm-12 text-center">
                                  <?php
                                  foreach ($rimages as $img) {
                                    ?>
                                    <a href="<?=MY_UPLOADURL.$img[COL_FILENAME]?>" data-toggle="lightbox" data-title="<?=$n[COL_POSTTITLE]?>" data-gallery="gallery">
                                      <img src="<?=MY_UPLOADURL.$img[COL_FILENAME]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                    </a>
                                    <?php
                                  }
                                  ?>
                                </div>
                              </div>
                              <?php
                            }
                            ?>
                            <br  />
                            <?php
                            $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                            ?>
                            <?=strlen($strippedcontent) > 400 ? substr($strippedcontent, 0, 400) . "..." : $strippedcontent ?>
                          </div>
                        </div>
                      </div>
                      <?php
                    }
                     ?>
                  </div>
                  <?php
                } else {
                  ?>
                  <p class="font-italic">
                    Tidak ada data untuk ditampilkan.
                  </p>
                  <?php
                }
                 ?>
              </div>
            </div>
            <div class="card-footer text-right">
              <a href="<?=site_url('site/home/post/1')?>" class="btn btn-danger"><i class="fad fa-list"></i> Selengkapnya (<strong><?=$count_berita?></strong>)</a>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card card-outline card-default">
            <div class="card-header">
              <h5 class="card-title">PETA PERSEBARAN COVID-19</h5>
            </div>
            <div class="card-body p-0">
              <img src="<?=MY_IMAGEURL.'covid-19-map.jpg'?>" width="100%" />
            </div>
            <div class="card-footer p-0">
              <table id="tbl-data-kec" class="table table-bordered mb-0">
                <thead>
                  <tr>
                    <th>Kecamatan</th>
                    <th>OTG</th>
                    <th>ODP</th>
                    <th>PDP</th>
                    <th>Positif</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            </div>

          <div class="card card-outline card-default">
            <div class="card-header">
              <h5 class="card-title">Info Grafis</h5>
            </div>
            <div class="card-body p-0">
              <?php
              if(!empty($grafis)) {
                $img = array();
                foreach ($grafis as $g) {
                  $rimages = $this->db->where(COL_POSTID, $g[COL_POSTID])->get(TBL__POSTIMAGES)->result_array();
                  foreach($rimages as $i) {
                    $img[] = array('src'=>MY_UPLOADURL.$i[COL_FILENAME],'caption'=>$g[COL_POSTTITLE]);
                  }
                }
                if(!empty($img)) {
                  ?>
                  <div id="carouselGrafis" class="carousel slide carousel-fade" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <?php
                      for($i=0; $i<count($img); $i++) {
                        ?>
                        <li data-target="#carouselGrafis" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                        <?php
                      }
                      ?>
                    </ol>
                    <div class="carousel-inner">
                      <?php
                      for($i=0; $i<count($img); $i++) {
                        ?>
                        <div class="carousel-item <?=$i==0?'active':''?>">
                          <img class="d-block w-100" src="<?=$img[$i]['src']?>">
                          <div class="carousel-caption"><?=$img[$i]['caption']?></div>
                        </div>
                        <?php
                      }
                      ?>
                    </div>
                    <a class="carousel-control-prev" href="#carouselGrafis" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselGrafis" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                  <?php
                }
                ?>
                <?php
              } else {
                ?>
                <p class="p-3 font-italic">
                  Tidak ada data untuk ditampilkan.
                </p>
                <?php
              }
               ?>
            </div>
            <div class="card-footer text-right">
              <a href="<?=site_url('site/home/post/3')?>" class="btn btn-danger"><i class="fad fa-list"></i> Selengkapnya (<strong><?=$count_grafis?></strong>)</a>
            </div>
          </div>
          <div class="card card-outline card-default">
            <div class="card-header">
              <h5 class="card-title">Pemberitahuan</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <?php
                if(!empty($notif)) {
                  ?>
                  <div class="timeline timeline-inverse mb-0 w-100">
                    <?php
                    foreach($notif as $n) {
                      ?>
                      <div class="row m-0 mb-2">
                        <div class="timeline-item m-0 w-100">
                          <span class="time"><i class="fad fa-calendar"></i> <?=date('d-m-Y', strtotime($n[COL_POSTDATE]))?></span>
                          <h3 class="timeline-header font-weight-bold"><a href="<?=site_url('site/home/page/'.$n[COL_POSTSLUG])?>"><?=$n[COL_POSTTITLE]?></a></h3>
                          <div class="timeline-body">
                            <?php
                            $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                            ?>
                            <?=strlen($strippedcontent) > 400 ? substr($strippedcontent, 0, 400) . "..." : $strippedcontent ?>
                          </div>
                        </div>
                      </div>
                      <?php
                    }
                     ?>
                  </div>
                  <?php
                } else {
                  ?>
                  <p class="font-italic">
                    Tidak ada data untuk ditampilkan.
                  </p>
                  <?php
                }
                 ?>
              </div>
            </div>
            <div class="card-footer text-right">
              <a href="<?=site_url('site/home/post/2')?>" class="btn btn-danger"><i class="fad fa-list"></i> Selengkapnya (<strong><?=$count_notif?></strong>)</a>
            </div>
          </div>
        </div>
        <div class="col-sm-12">

        </div>
      </div>
    </div>
  </section>
</section>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript">
function getData() {
  $('.text-data').html('<i class="fad fa-sync-alt fa-spin"></i>');
  /*$.get("<?=site_url('site/ajax/getdata')?>", function(data) {
    var res = JSON.parse(data);
    if(res) {
      $('.text-data-odp').html($.number(parseInt(res.Jlh_ODP_Pantau) + parseInt(res.Jlh_ODP_Selesai)));
      $('.text-data-odp-p').html($.number(parseInt(res.Jlh_ODP_Pantau)));
      $('.text-data-odp-s').html($.number(parseInt(res.Jlh_ODP_Selesai)));

      $('.text-data-otg').html($.number(parseInt(res.Jlh_OTG_Pantau) + parseInt(res.Jlh_OTG_Selesai)));
      $('.text-data-otg-p').html($.number(parseInt(res.Jlh_OTG_Pantau)));
      $('.text-data-otg-s').html($.number(parseInt(res.Jlh_OTG_Selesai)));

      $('.text-data-pdp').html($.number(parseInt(res.Jlh_PDP_Pantau) + parseInt(res.Jlh_PDP_Sehat)));
      $('.text-data-pdp-p').html($.number(parseInt(res.Jlh_PDP_Pantau)));
      $('.text-data-pdp-s').html($.number(parseInt(res.Jlh_PDP_Sehat)));

      $('.text-data-suspect').html($.number(parseInt(res.Jlh_Suspect_Passed) + parseInt(res.Jlh_Suspect_Recovered)));
      $('.text-data-suspect-d').html($.number(parseInt(res.Jlh_Suspect_Passed)));
      $('.text-data-suspect-r').html($.number(parseInt(res.Jlh_Suspect_Recovered)));

      $('.text-data-last-updatedate').html(moment(res.Create_Date).format('DD-MM-YYYY'));
      $('.text-data-last-updatetime').html(moment(res.Create_Date).format('H:mm'));
      $('.text-data-last-updateby').html(res.Name);
    } else {
      $('.text-data').html('--');
    }
  });*/
  $.get("<?=site_url('site/ajax/getdata-kec')?>", function(data) {
    var res = JSON.parse(data);
    if(res) {
      $('.text-data-otg-p').html($.number(parseInt(res.Jlh_OTGP)));
      $('.text-data-otg-s').html($.number(parseInt(res.Jlh_OTGS)));
      $('.text-data-odp-p').html($.number(parseInt(res.Jlh_ODP)));
      $('.text-data-odp-s').html($.number(parseInt(res.Jlh_ODPS)));
      $('.text-data-pdp').html($.number(parseInt(res.Jlh_PDP)));
      $('.text-data-positif').html($.number(parseInt(res.Jlh_Positif)));

      $('.text-data-suspect').html($.number(parseInt(res.Jlh_Suspect)));
      $('.text-data-probable').html($.number(parseInt(res.Jlh_Probable)));
      $('.text-data-kontakerat').html($.number(parseInt(res.Jlh_KontakErat)));
      $('.text-data-pelakuperjalanan').html($.number(parseInt(res.Jlh_PelakuPerjalanan)));
      $('.text-data-discarded').html($.number(parseInt(res.Jlh_Discarded)));
      $('.text-data-selesaiisolasi').html($.number(parseInt(res.Jlh_SelesaiIsolasi)));
      $('.text-data-kematian').html($.number(parseInt(res.Jlh_Kematian)));

      if(res.Create_Date) {
        $('.text-data-last-updatedate').html(moment(res.Create_Date).format('DD-MM-YYYY'));
        $('.text-data-last-updatetime').html(moment(res.Create_Date).format('H:mm'));
      } else {
        $('.text-data-last-updatedate').html('--');
        $('.text-data-last-updatetime').html('--');
      }

      $('.text-data-last-updateby').html(res.Name);
    } else {
      $('.text-data').html('--');
    }
  });

  $('#tbl-data-kec tbody').load("<?=site_url('site/ajax/getpersebaran-kec')?>", function() {

  });
}
$(document).ready(function() {
  getData();
  setInterval(function(){
    getData();
  }, 60000);

  $(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox({
      alwaysShowClose: true
    });
  });
});
</script>
