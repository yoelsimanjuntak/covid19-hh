<?php
$ruser = GetLoggedUser();

 ?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?=$title?></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <!--<div id="card-data" class="card card-outline card-danger">
          <div class="card-header">
            <h2 class="card-title">Rekapitulasi Data</h2>
            <div class="card-tools">
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i></button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Kategori</label>
                    <select class="form-control" name="<?=COL_JENIS?>">
                      <option value="MASUK">MASUK</option>
                      <option value="KELUAR">KELUAR</option>
                      <option value="KELUAR">LAPOR</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Disposisi</label>
                    <select class="form-control" name="<?=COL_STATUS?>">
                      <option value="VERIFIKASI" selected>VERIFIKASI</option>
                      <option value="PEMANTAUAN">PEMANTAUAN</option>
                      <option value="PEMANTAUAN_SELESAI">PEMANTAUAN SELESAI</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name="<?=COL_STATUS_AKHIR?>">
                      <option value="" selected>KOSONG</option>
                      <option value="OTG">OTG</option>
                      <option value="ODP">ODP</option>
                      <option value="PDP">PDP</option>
                      <option value="POSITIF">POSITIF</option>
                    </select>
                  </div>
                </div>
              </div>

              <table id="datalist" class="table table-bordered" style="white-space: nowrap;">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Kategori</th>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Kecamatan</th>
                    <th>Desa</th>
                    <th>Disposisi</th>
                    <th>Status Akhir</th>
                  </tr>
                </thead>
              </table>
            </form>
          </div>
          <div class="overlay dark" style="display: none">
            <i class="fad fa-2x fa-sync-alt fa-spin"></i>
          </div>
        </div>-->
        <div id="card-data" class="card card-outline card-danger">
          <div class="card-header">
            <h2 class="card-title">Data COVID-19</h2>
            <div class="card-tools">
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i></button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered" style="white-space: nowrap;">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Kecamatan</th>
                      <th>Tanggal</th>
                      <th>Oleh</th>
                      <th>Suspect</th>
                      <th>Probable</th>
                      <th>Kontak Erat</th>
                      <th>Pelaku Perjalanan</th>
                      <th>Discarded</th>
                      <th>Selesai Isolasi</th>
                      <th>Kematian</th>
                    </tr>
                  </thead>
                </table>
            </form>
          </div>
          <div class="overlay dark" style="display: none">
            <i class="fad fa-2x fa-sync-alt fa-spin"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Data Kecamatan : <span id="name-kecamatan"></span></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="far fa-sm fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-editor" method="post" action="#">
          <!--<div class="form-group row">
              <label class="control-label col-sm-2 mt-4">OTG</label>
              <div class="col-sm-5">
                <p class="help-block mb-0 font-italic text-warning">Dalam Pemantauan</p>
                <input type="text" class="form-control text-right uang" name="<?=COL_JLH_OTGP?>" required />

              </div>
              <div class="col-sm-5">
                <p class="help-block mb-0 font-italic text-success">Selesai Pemantauan</p>
                <input type="text" class="form-control text-right uang" name="<?=COL_JLH_OTGS?>" required />
              </div>
          </div>
          <div class="form-group row">
              <label class="control-label col-sm-2 mt-4">ODP</label>
              <div class="col-sm-5">
                <p class="help-block mb-0 font-italic text-warning">Dalam Pemantauan</p>
                <input type="text" class="form-control text-right uang" name="<?=COL_JLH_ODP?>" required />

              </div>
              <div class="col-sm-5">
                <p class="help-block mb-0 font-italic text-success">Selesai Pemantauan</p>
                <input type="text" class="form-control text-right uang" name="<?=COL_JLH_ODPS?>" required />
              </div>
          </div>
          <div class="form-group row">
              <label class="control-label col-sm-2">PDP</label>
              <div class="col-sm-5">
                <input type="text" class="form-control text-right uang" name="<?=COL_JLH_PDP?>" required />
              </div>
          </div>
          <div class="form-group row">
              <label class="control-label col-sm-2">Positif</label>
              <div class="col-sm-5">
                <input type="text" class="form-control text-right uang" name="<?=COL_JLH_POSITIF?>" required />
              </div>
          </div>-->
          <div class="form-group row">
            <div class="col-sm-6">
              <label class="control-label">Suspect</label>
              <input type="text" class="form-control text-right uang" name="<?=COL_JLH_SUSPECT?>" required />
            </div>
            <div class="col-sm-6">
              <label class="control-label">Probable</label>
              <input type="text" class="form-control text-right uang" name="<?=COL_JLH_PROBABLE?>" required />
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
              <label class="control-label">Kontak Erat</label>
              <input type="text" class="form-control text-right uang" name="<?=COL_JLH_KONTAKERAT?>" required />
            </div>
            <div class="col-sm-6">
              <label class="control-label">Pelaku Perjalanan</label>
              <input type="text" class="form-control text-right uang" name="<?=COL_JLH_PELAKUPERJALANAN?>" required />
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
              <label class="control-label">Discarded</label>
              <input type="text" class="form-control text-right uang" name="<?=COL_JLH_DISCARDED?>" required />
            </div>
            <div class="col-sm-6">
              <label class="control-label">Selesai Isolasi</label>
              <input type="text" class="form-control text-right uang" name="<?=COL_JLH_SELESAIISOLASI?>" required />
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
              <label class="control-label">Kematian</label>
              <input type="text" class="form-control text-right uang" name="<?=COL_JLH_KEMATIAN?>" required />
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btn-ok">Submit</button>
      </div>
    </div>
  </div>
</div>
<!--<div class="modal fade" id="modal-popup" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Detail</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="far fa-sm fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>-->
<script>
$(document).ready(function() {
  var dataTable = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      url : "<?=site_url('site/data/covid19-load-kec')?>",
      type : 'POST'
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columnDefs": [{"targets":[4,5,6,7,8,9], "className":'text-center'}],
    "columns": [
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-add-data', $(row)).click(function(){
        var a = $(this);
        var editor = $("#modal-editor");

        $("#name-kecamatan", editor).html(a.data('kec'));
        editor.modal("show");
        $(".btn-ok", editor).unbind('click').click(function() {
          var dis = $(this);
          dis.html("Loading...").attr("disabled", true);
          $('#form-editor').ajaxSubmit({
            dataType: 'json',
            url : a.attr('href'),
            success : function(data){
              if(data.error==0){
                toastr.success(data.success);
              }else{
                toastr.error(data.error);
              }
            },
            error: function(e) {
              toastr.error('Server Error.');
            },
            complete: function() {
              dis.html('Submit').attr("disabled", false);
              editor.modal("hide");
              $('#datalist').DataTable().ajax.reload();
            }
          });
        });
        return false;
      });
    }
  });
  $("#modal-editor").on("hidden.bs.modal", function(){
    $('input', $('#form-editor', $("#modal-editor"))).val(0);
  });
  $('#cekbox').click(function(){
      if($(this).is(':checked')){
          $('.cekbox').prop('checked',true);
      }else{
          $('.cekbox').prop('checked',false);
      }
  });

  $('.btn-refresh-data', $('#card-data')).unbind().click(function() {
    $('#datalist').DataTable().ajax.reload();
  });
  /*var dataTable = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/data/covid19-load-detail')?>",
      "type": 'POST',
      "data": function (d) {
        d.jenis = $('[name=<?=COL_JENIS?>]').val();
        d.status = $('[name=<?=COL_STATUS?>]').val();
        d.status_akhir = $('[name=<?=COL_STATUS?>]').val();
      }
    },
    "scrollY" : '30vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-12 text-right'l>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columnDefs": [{"targets":[0,1,6,7], "className":'text-center'}],
    "columns": [
      {"orderable": false},
      {"orderable": false},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false},
      {"orderable": false},
      //{"orderable": false},
      //{"orderable": false}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.link-popup', row).click(function(e) {
        var href = $(this).attr('href');
        var modal = $("#modal-popup");

        modal.modal('show');
        $('.modal-body', modal).html('Loading...').load(href, function() {
          $("select", modal).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
        });

        e.preventDefault();
      });
    }
  });
  $("#modal-editor").on("hidden.bs.modal", function(){
    //$('input', $('#form-editor', $("#modal-editor"))).val(0);
  });
  $('#cekbox').click(function(){
      if($(this).is(':checked')){
        $('.cekbox').prop('checked',true);
      }else{
        $('.cekbox').prop('checked',false);
      }
  });

  $('.btn-refresh-data', $('#card-data')).unbind().click(function() {
    $('#datalist').DataTable().ajax.reload();
  });
  $('input, select', $('#dataform')).change(function() {
    $('#datalist').DataTable().ajax.reload();
  });

  $("#modal-popup").on("hidden.bs.modal", function(){
    $('.modal-body', $("#modal-popup")).empty();
  });*/
});
</script>
