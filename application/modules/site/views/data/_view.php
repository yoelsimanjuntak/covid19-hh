<div class="row">
  <div class="col-sm-12">
    <table class="table table-bordered text-sm">
      <tbody>
        <tr>
          <td class="nowrap text-right" width="100px">Kategori</td><td width="10px">:</td>
          <td><?=$data[COL_JENIS]?></td>
        </tr>
        <tr>
          <td class="nowrap text-right" width="100px">Disposisi</td><td width="10px">:</td>
          <td><?=$data[COL_STATUS]?></td>
        </tr>
        <tr>
          <td class="nowrap text-right" width="100px">Status Akhir</td><td width="10px">:</td>
          <td><?=!empty($data[COL_STATUS_AKHIR])?$data[COL_STATUS_AKHIR]:'-'?></td>
        </tr>
        <tr>
          <td class="nowrap text-right" width="100px">Catatan Akhir</td><td width="10px">:</td>
          <td><?=!empty($data[COL_CATATAN_AKHIR])?$data[COL_CATATAN_AKHIR]:'-'?></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-sm-6">
    <div class="card card-outline card-primary">
      <div class="card-header">
        <h5 class="card-title font-weight-light">Biodata</h5>
      </div>
      <div class="card-body p-0">
        <table class="table table-striped">
          <tbody>
            <tr>
              <td class="nowrap">Nama</td><td>:</td>
              <td><?=$data[COL_NAMA]?></td>
            </tr>
            <tr>
              <td class="nowrap">NIK</td><td>:</td>
              <td><?=$data[COL_NIK]?></td>
            </tr>
            <tr>
              <td class="nowrap">No. HP</td><td>:</td>
              <td><?=$data[COL_NO_HP]?></td>
            </tr>
            <tr>
              <td class="nowrap">Tempat Lahir</td><td>:</td>
              <td><?=$data[COL_TEMPAT_LAHIR] ?></td>
            </tr>
            <tr>
              <td class="nowrap">Tanggal Lahir</td><td>:</td>
              <td><?=date('d-m-Y', strtotime($data[COL_TGL_LAHIR])) ?></td>
            </tr>
            <tr>
              <td class="nowrap">Alamat</td><td>:</td>
              <td><?=$data[COL_ALAMAT].'<br />Kec. '.(!empty($data[COL_V_KECAMATAN])?$data[COL_V_KECAMATAN]:'-').'<br />Desa '.(!empty($data[COL_V_DESA])?$data[COL_V_DESA]:'-') ?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card card-outline card-danger">
      <div class="card-header">
        <h5 class="card-title font-weight-light">Info Medis</h5>
      </div>
      <div class="card-body p-0">
        <table class="table table-striped">
          <tbody>
            <tr>
              <td class="nowrap">Suhu Badan</td><td>:</td>
              <td><?=!empty($data[COL_SUHU_BADAN])?$data[COL_SUHU_BADAN]:'-'?></td>
            </tr>
            <tr>
              <td class="nowrap">Gejala</td><td>:</td>
              <td class="nowrap">
                <?php
                $arrKesehatan = explode(',', $data[COL_KESEHATAN]);
                if(empty($arrKesehatan)) {
                  echo '-';
                } else {
                  ?>
                  <ul class="pl-0" style="list-style-type:none">
                    <?php
                    foreach ($arrKesehatan as $kes) {
                      ?>
                      <li><?=$kes?></li>
                      <?php
                    }
                    ?>
                  </li>
                  <?php
                }
                ?>
              </td>
            </tr>
            <tr>
              <td class="nowrap">Catatan</td><td>:</td>
              <td><?=!empty($data[COL_CATATAN_MEDIS])?$data[COL_CATATAN_MEDIS]:'-'?></td>
            </tr>
            <tr>
              <td class="nowrap">Rujukan</td><td>:</td>
              <td><?=!empty($data[COL_V_PUSKESMAS])?$data[COL_V_PUSKESMAS]:'-'?></td>
            </tr>
            <tr>
              <td class="nowrap">Diagnosa</td><td>:</td>
              <td><?=!empty($data[COL_DIAGNOSA])?$data[COL_DIAGNOSA]:'-'?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
