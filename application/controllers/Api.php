<?php
class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function tampungJson()
    {
    	$json = file_get_contents('php://input');
		$data = json_decode($json);


		if($json)
		{

		    //var_dump($data);
		    //disini semua action yg datang//
		    $id = $data->id;
		    $wak = date('YmdHi');

		    if($data->foto_ktp != "")
		    {
		    	$base64_ktp = $data->foto_ktp;
            	$data->foto_ktp = $this->base64_to_jpeg( $base64_ktp, 'uploads/'.$id.'_ktp.jpg' );
		    }else{
		    	unset($data->foto_ktp);
		    }

		    if($data->foto_orang != "")
		    {
		    	$base64_orang = $data->foto_orang;
            	$data->foto_orang = $this->base64_to_jpeg( $base64_orang, 'uploads/'.$id.'_orang.jpg' );
		    }else{
		    	unset($data->foto_orang);
		    }

        $this->db->select('id');
			$this->db->where('id',$id);
			$query = $this->db->get('covid19_data_detail');
			$num = $query->num_rows();
			if($num>0)
			{
				$this->db->set($data);
				$this->db->where('id',$id);
				$this->db->update('covid19_data_detail');
			}else{
				$this->db->insert('covid19_data_detail',$data);
			}

		}else{
		   die("Harus Json");
		}

    }


    private function base64_to_jpeg( $base64_string, $output_file )
	{
	        $ifp = fopen( $output_file, "wb" );
	        fwrite( $ifp, base64_decode( $base64_string) );
	        fclose( $ifp );
	        return( $output_file );
	}


	public function panggil_by_status($status="")
	{
		if($status=="")
		{
			die("status required");
		}

		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: *");
		header('Content-Type: application/json');


		$q = $this->db->query("SELECT * FROM covid19_data_detail WHERE status='$status' AND jenis='MASUK'");
		echo json_encode($q->result());

	}



	public function hitung()
	{

		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: *");
		header('Content-Type: application/json');


		$q_masuk = $this->db->query("SELECT id FROM covid19_data_detail WHERE jenis='MASUK'");
		$data['masuk'] = $q_masuk->num_rows();

		$q_keluar = $this->db->query("SELECT id FROM covid19_data_detail WHERE jenis='KELUAR'");
		$data['keluar'] = $q_keluar->num_rows();


		$q_odp = $this->db->query("SELECT id FROM covid19_data_detail WHERE status_akhir='ODP' AND jenis='MASUK'");
		$data['ODP'] = $q_odp->num_rows();

		$q_otg = $this->db->query("SELECT id FROM covid19_data_detail WHERE status_akhir='OTG' AND jenis='MASUK'");
		$data['OTG'] = $q_otg->num_rows();

		$q_positif = $this->db->query("SELECT id FROM covid19_data_detail WHERE status_akhir='POSITIF' AND jenis='MASUK'");
		$data['POSITIF_COVID19'] = $q_positif->num_rows();


		$q_verif = $this->db->query("SELECT id FROM covid19_data_detail WHERE status='Verifikasi' AND jenis='MASUK'");
		$data['Verifikasi'] = $q_verif->num_rows();


		$q_pemantauan = $this->db->query("SELECT id FROM covid19_data_detail WHERE status='Pemantauan' AND jenis='MASUK'");
		$data['Pemantauan'] = $q_pemantauan->num_rows();


		$q_sakit = $this->db->query("SELECT id FROM covid19_data_detail WHERE status='Sakit' AND jenis='MASUK'");
		$data['Sakit'] = $q_sakit->num_rows();


		$q_selesai = $this->db->query("SELECT id FROM covid19_data_detail WHERE status='Selesai' OR status_akhir='PELAKU_PERJALANAN'");
		$data['Selesai'] = $q_selesai->num_rows();


		echo json_encode($data);

	}
}
?>
