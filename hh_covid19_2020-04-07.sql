# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: hh_covid19
# Generation Time: 2020-04-07 14:36:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table covid19_kecamatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `covid19_kecamatan`;

CREATE TABLE `covid19_kecamatan` (
  `Kd_Kecamatan` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nm_Kecamatan` varchar(50) DEFAULT NULL,
  `Nm_Color` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Kd_Kecamatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `covid19_kecamatan` WRITE;
/*!40000 ALTER TABLE `covid19_kecamatan` DISABLE KEYS */;

INSERT INTO `covid19_kecamatan` (`Kd_Kecamatan`, `Nm_Kecamatan`, `Nm_Color`)
VALUES
	(1,'DOLOKSANGGUL',NULL),
	(2,'LINTONG NI HUTA',NULL),
	(3,'BAKTIRAJA',NULL),
	(4,'POLLUNG',NULL),
	(5,'PARLILITAN',NULL),
	(6,'PARANGINAN',NULL),
	(7,'PAKKAT',NULL),
	(8,'ONAN GANJANG',NULL),
	(9,'TARABINTANG',NULL),
	(10,'SIJAMAPOLANG',NULL);

/*!40000 ALTER TABLE `covid19_kecamatan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table covid19_kecamatan_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `covid19_kecamatan_data`;

CREATE TABLE `covid19_kecamatan_data` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Kd_Kecamatan` bigint(10) unsigned NOT NULL,
  `Jlh_OTGP` double DEFAULT '0',
  `Jlh_OTGS` double DEFAULT '0',
  `Jlh_ODP` double DEFAULT '0',
  `Jlh_ODPS` double DEFAULT '0',
  `Jlh_PDP` double DEFAULT '0',
  `Jlh_Positif` double DEFAULT '0',
  `Create_By` varchar(50) NOT NULL DEFAULT '',
  `Create_Date` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_Kecamatan` (`Kd_Kecamatan`),
  CONSTRAINT `FK_Kecamatan` FOREIGN KEY (`Kd_Kecamatan`) REFERENCES `covid19_kecamatan` (`Kd_Kecamatan`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
